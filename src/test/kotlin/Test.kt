import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Test {
    @Test fun `Given two identical strings verify true`() {
        val s1 = "abcd"
        val s2 = "abcd"
        assertTrue(isOneOrZeroEditsAway(s1, s2))
    }

    @Test fun `Given two strings of the same length with one difference verify true`() {
        var s1 = "abcd"
        val s2 = "abce"
        assertTrue(isOneOrZeroEditsAway(s1, s2))
    }

    @Test fun `Given two strings of the same length with two differences verify false`() {
        var s1 = "abcde"
        val s2 = "abcef"
        assertFalse(isOneOrZeroEditsAway(s1, s2))
    }

    @Test fun `Given two strings differing in length by more than one verify false`() {
        var s1 = "abcde"
        val s2 = "abcefgh"
        assertFalse(isOneOrZeroEditsAway(s1, s2))
    }

    @Test fun `Given two strings differing in length by one verify false`() {
        var s1 = "abcde"
        val s2 = "abcefg"
        assertFalse(isOneOrZeroEditsAway(s1, s2))
    }

    @Test fun `Given two strings differing in length by one verify true`() {
        var s1 = "abcde"
        val s2 = "abcdeg"
        assertFalse(isOneOrZeroEditsAway(s1, s2))
    }

}
