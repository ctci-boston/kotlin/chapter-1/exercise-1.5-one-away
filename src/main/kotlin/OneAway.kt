import kotlin.math.abs

/**
 * Exercise 1.5 One Away: There are three types of edits that can be performed on strings:
 * insert a character, remove a character, or replace a character. Given two strings, write
 * a function to check if they are one edit (or zero edits) away.
 */
fun isOneOrZeroEditsAway(s1: String, s2: String): Boolean {
    fun isOneOrZeroEditsAway(): Boolean {
        var numberOfReplaces = 0
        fun updateNumberOfReplaces(index: Int): Int {
            if (s1[index] != s2[index]) numberOfReplaces++
            return numberOfReplaces
        }

        for (i in s1.indices) { if (updateNumberOfReplaces(i) > 1) return false }
        return true
    }
    fun canUseRemove(): Boolean {
        val larger = if (s1.length >= s2.length) s1 else s2
        val smaller = if (s1.length < s2.length) s2 else s1

        for (i in smaller.indices) {
            if (larger.replaceRange(i, i + 1, "") == smaller) return true
        }
        return false
    }

    return when {
        s1.length == s2.length -> isOneOrZeroEditsAway()
        abs(s1.length - s2.length) > 1 -> false
        else -> canUseRemove()
    }
}
